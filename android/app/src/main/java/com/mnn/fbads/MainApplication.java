package com.mnn.fbads;
import android.content.Context;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import com.facebook.react.ReactApplication;
import ru.simaLand.react.cookie.CookieManagerPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.wheelpicker.WheelPickerPackage;
import com.th3rdwave.safeareacontext.SafeAreaContextPackage;
import com.microsoft.codepush.react.CodePush;
import com.swmansion.reanimated.ReanimatedPackage;
import com.reactnativecommunity.cameraroll.CameraRollPackage;
import fr.bamlab.rnimageresizer.ImageResizerPackage;
import com.github.yamill.orientation.OrientationPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.horcrux.svg.SvgPackage;
import com.reactnativecommunity.netinfo.NetInfoPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.microsoft.appcenter.reactnative.analytics.AppCenterReactNativeAnalyticsPackage;
import com.microsoft.appcenter.reactnative.appcenter.AppCenterReactNativePackage;
import com.microsoft.appcenter.reactnative.crashes.AppCenterReactNativeCrashesPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.wheelpicker.WheelPickerPackage;

import java.util.Arrays;
import java.util.List;

import cl.json.RNSharePackage;
import cl.json.ShareApplication;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;

 public class MainApplication extends MultiDexApplication implements ReactApplication, ShareApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    protected String getJSBundleFile(){
      if (!BuildConfig.DEBUG) {
        return CodePush.getJSBundleFile();
      }else{
        return super.getJSBundleFile();
      }
    }

    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
              new MainReactPackage(),
            new CookieManagerPackage(),
            new RNCWebViewPackage(),
            new WheelPickerPackage(),
            new SafeAreaContextPackage(),
            new SafeAreaContextPackage(),
            new ReanimatedPackage(),
            new CameraRollPackage(),
            new ImageResizerPackage(),
            new OrientationPackage(),
            new VectorIconsPackage(),
            new SvgPackage(),
            new NetInfoPackage(),
              new WheelPickerPackage(),
              new CodePush(getResources().getString(R.string.reactNativeCodePush_androidDeploymentKey), getApplicationContext(), BuildConfig.DEBUG),
              new AppCenterReactNativeCrashesPackage(MainApplication.this, getResources().getString(R.string.appCenterCrashes_whenToSendCrashes)),
              new AppCenterReactNativeAnalyticsPackage(MainApplication.this, getResources().getString(R.string.appCenterAnalytics_whenToEnableAnalytics)),
              new AppCenterReactNativePackage(MainApplication.this),
              new PickerPackage(),
              new RNSharePackage(),
              new RNDeviceInfo(),
              new AsyncStoragePackage(),
              new RNGestureHandlerPackage()

      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }

  @Override
  public String getFileProviderAuthority() {
         return "com.mnn.fbads.provider";
  }

   @Override
   protected void attachBaseContext(Context base) {
     super.attachBaseContext(base);
     MultiDex.install(base);
   }
}
