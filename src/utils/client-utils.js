import axios from 'axios';
import { Platform } from 'react-native';
// const server_url = 'https://dhy2.demo.isofh.vn'; //release
const server_url = 'https://api.ivisitor.vn'; //release
// const server_url = 'http://123.24.206.9:2182'; //release


const resource_url = server_url + "/api/visitor/v1/files/"; //release
import navigationUtils from "@utils/navigation-utils";
import snackbar from "@utils/snackbar-utils";

const httpClient = axios.create();
httpClient.defaults.timeout = 50000;

String.prototype.absoluteUrl =
  String.prototype.absolute ||
  function(defaultValue) {
    var _this = this.toString();
    if (_this == "")
      if (defaultValue != undefined) return defaultValue;
      else return _this;

    if (
      _this.indexOf("http") == 0 ||
      _this.indexOf("file") == 0 ||
      _this.indexOf("content") == 0 ||
      _this.indexOf("blob") == 0
    ) {
      return _this;
    }
    let _this2 = _this.toLowerCase();
    if (
      _this2.endsWith("jpg") ||
      _this2.endsWith("png") ||
      _this2.endsWith("gif") ||
      _this2.endsWith("jpeg")
    ) {
      let image = resource_url + _this;
      return image;
    }
    if (
      !_this2.endsWith(".jpg") ||
      !_this2.endsWith(".png") ||
      !_this2.endsWith(".gif")
    ) {
      return defaultValue;
    }
    // if(this.startsWith("user"))

    //     return
    return server_url + _this;
  };

String.prototype.getServiceUrl =
  String.prototype.absolute ||
  function(defaultValue) {
    let _this = this ? this.toString() : "";
    if (_this == "")
      if (defaultValue != undefined) return defaultValue;
      else return _this;
    if (_this.indexOf("http") == 0 || _this.indexOf("blob") == 0) {
      return _this;
    }
    return server_url + "/" + _this;
  };

module.exports = {
  auth: "",
  lang: "vi",
  serverApi: server_url + "/",
  uploadFile(url, file) {
    const formData = new FormData();
    if (Array.isArray(file)) {
      file.forEach((item) => {
        formData.append("file", {
          uri: item,
          type: "image/jpeg", // or photo.type
          name: Math.floor(Math.random() * Math.floor(999999999)) + ".jpg",
        });
      });
    } else {
      formData.append("file", {
        uri: file,
        type: "image/jpeg", // or photo.type
        name: Math.floor(Math.random() * Math.floor(999999999)) + ".jpg",
      });
    }
    const config = {
      headers: {
        // 'content-type': 'multipart/form-data',
        Authorization: this.auth ? "Bearer " + this.auth : "",
        // 'MobileMode': 'vendorPortal'
      },
    };
    console.log(url.getServiceUrl());
    return axios.post(url.getServiceUrl(), formData, config);
  },

  uploadFileDoc(url, data, funRes) {
    this.requestFetch(
      "post",
      this.serverApi + url,
      {
        Accept: "application/json",
        "Content-Type": "multipart/form-data",
        Authorization: this.auth ? "Bearer " + this.auth : "",
        "Accept-Language": this.lang || "vi",
      },
      data
    )
      .then((s) => {
        if (funRes) {
          if (s.data) {
            funRes(s.data);
          } else {
            funRes(undefined, e);
          }
        }
      })
      .catch((e) => {
        if (funRes) funRes(undefined, e);
      });
  },
  requestApi(methodType, url, body) {
    return this.requestApiWithAuthorization(methodType, url, body, this.auth);
  },
  requestApiWithAuthorization(methodType, url, body, auth) {
    return new Promise((resolve, reject) => {
      var dataBody = "";
      if (!body) body = {};
      dataBody = JSON.stringify(body);

      this.requestFetch(
        methodType,
        url && url.indexOf("http") == 0 ? url : this.serverApi + url,
        {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: this.auth ? "Bearer " + this.auth : "",
          "Accept-Language": this.lang || "vi",
        },
        dataBody
      )
        .then((s) => {
          if (s.data) {
            if (s.data && s.data.code == 401) {
              navigationUtils.reset("login");
              return;
            }
            resolve(s.data);
          } else {
            reject(s);
          }
        })
        .catch((e) => {
          snackbar.show(e.message, "danger");
          reject(e);
        });
    });
  },

  requestApiWithHeader(methodType, url, body, header) {
    return new Promise((resolve, reject) => {
      var dataBody = "";
      if (!body) body = {};
      dataBody = JSON.stringify(body);
      let headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: this.auth ? "Bearer " + this.auth : "",
        "Accept-Language": this.lang || "vi",
      };
      if (!header) header = {};
      for (var key in header) {
        headers[key] = header[key];
      }
      this.requestFetch(
        methodType,
        url && url.indexOf("http") == 0 ? url : this.serverApi + url,
        headers,
        dataBody
      )
        .then((s) => {
          if (s.data) {
            resolve(s.data);
          } else {
            reject(s);
          }
        })
        .catch((e) => {
          reject(e);
        });
    });
  },
  requestFetch(methodType, url, headers, body) {
    let data = {
      methodType,
      url: url.getServiceUrl(),
      headers,
      body,
    };
    console.log(this.lang);
    console.log(JSON.stringify(data));
    return new Promise((resolve, reject) => {
      let promise1 = null;
      switch ((methodType || "").toLowerCase()) {
        case "post":
          promise1 = httpClient.post(url.getServiceUrl().toString(), body, {
            headers,
          });
          break;
        case "get":
          promise1 = httpClient.get(url.getServiceUrl().toString(), {
            headers,
          });
          break;
        case "put":
          promise1 = httpClient.put(url.getServiceUrl().toString(), body, {
            headers,
          });
          break;
        case "delete":
          promise1 = httpClient.delete(url.getServiceUrl().toString(), {
            headers,
          });
          break;
      }

      promise1
        .then((json) => {
          console.log(json);
          if (json.status != 200) {
            reject(json);
          } else resolve(json);
        })
        .catch((e) => {
          if (e && e.request) {
            console.log(e.request);
          } else if (e && e.response) {
            console.log(e.response);
          } else {
            console.log(e);
          }
          reject(e);
        });
    });
  },
};
