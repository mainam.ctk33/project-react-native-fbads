import themes from '@themes';

const reducer = (state = {}, action) => {
	let newState = { ...state };
	switch (action.type) {
		case 'SET_THEME':
			newState.themeType = action.theme;
			themes.theme = action.theme;
			newState.theme = themes.getTheme(action.theme);
			return newState;
		case 'persist/REHYDRATE':
			if (action.payload && action.payload.theme && action.payload.theme.themeType) {
				newState.themeType = action.payload.theme.themeType;
				newState.theme = themes.getTheme(newState.themeType);
				return newState;
			} else {
				newState.themeType = "normal";
				themes.theme = newState.themeType;
				newState.theme = themes.getTheme(newState.themeType);
				return newState;
			}
		default:
			return state
	}

}
export default reducer
