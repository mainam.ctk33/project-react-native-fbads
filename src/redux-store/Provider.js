/* eslint-disable semi */
/* eslint-disable prettier/prettier */
import React from 'react'
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import { PersistGate } from 'redux-persist/integration/react';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import thunk from 'redux-thunk';
import AppReducer from '@redux';
const persistConfig = {
    key: 'persistKey',
    // keyPrefix: 'x', // the redux-persist default `persist:` doesn't work with some file systems
    storage: AsyncStorage,
};
const store = createStore(
    persistReducer(persistConfig, AppReducer),
    {},
    compose(applyMiddleware(thunk)),
);
store.__PERSISTOR = persistStore(store);


export default function MyProvider(props) {
    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={store.__PERSISTOR}>
                {props.children}
            </PersistGate>
        </Provider>
    )
}
