import datacacheProvider from "@data-access/datacache-provider";

export function setTheme(theme) {
	return {
		type: 'SET_THEME',
		theme
	}
}

export default {
	setTheme
}