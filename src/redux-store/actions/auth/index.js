import userProvider from "@data-access/user-provider";
import navigationUtils from "@utils/navigation-utils";
import snackbar from "@utils/snackbar-utils";
export function saveCurrentUser(user, saveMe) {
  return {
    type: "AUTH_SAVE_CURRENT_USER",
    data: { user, saveMe },
  };
}

export function logout() {
  return {
    type: "AUTH_LOGOUT",
  };
}

export function updateData(data) {
  return {
    type: "AUTH_UPDATE_DATA",
    data,
  };
}

export default {
  saveCurrentUser,
  updateData,
};
