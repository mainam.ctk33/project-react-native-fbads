import { combineReducers } from "redux";
import auth from "@reducers/auth";
import theme from "@reducers/theme";

export default combineReducers({
  auth,
  theme,
});
