/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from "react";
import { StatusBar, View, TouchableOpacity, Text } from "react-native";
const App = (props) => {
  const loginFacebook = () => {
    props.navigation.navigate("facebook");
  };
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <StatusBar hidden={true} />
      <TouchableOpacity
        onPress={loginFacebook}
        style={{
          backgroundColor: "#2D88FF",
          borderRadius: 20,
          padding: 15,
          paddingHorizontal: 30,
        }}
      >
        <Text style={{ fontWeight: "bold", color: "#FFF" }}>
          Đăng nhập facebook
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default App;
