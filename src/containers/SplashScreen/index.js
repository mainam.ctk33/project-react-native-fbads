import React, { Component } from "react";
import { StatusBar, View } from "react-native";
import { connect } from "react-redux";
import ScaleImage from "mainam-react-native-scaleimage";
import * as Animatable from "react-native-animatable";
import actionTheme from "@actions/theme";
import navigationUtils from "@utils/navigation-utils";
import ActivityPanel from "@components/ActivityPanel";
const MyScaleImage = Animatable.createAnimatableComponent(ScaleImage);

class SplashScreen extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    console.disableYellowBox = true;
    console.reportErrorsAsExceptions = false;
    setTimeout(() => {
      if (this.props.auth) {
        navigationUtils.replace("facebook");
      } else {
        navigationUtils.replace("home");
      }
    }, 3000);
  }

  render() {
    return (
      <ActivityPanel
        hideActionbar={true}
        hideStatusbar={true}
        showFullScreen={true}
        showBackgroundHeader={false}
      >
        <StatusBar hidden={true} />
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            width: this.props.theme.Metrics.width,
            height: this.props.theme.Metrics.height + 100,
            resizeMode: "stretch",
          }}
        >
          <MyScaleImage
            animation="zoomIn"
            delay={500}
            duration={3000}
            source={this.props.theme.Images.logo}
            width={200}
          />
        </View>
      </ActivityPanel>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth.auth,
    theme: state.theme.theme,
  };
}
export default connect(
  mapStateToProps,
  {
    setTheme: actionTheme.setTheme,
  }
)(SplashScreen);
