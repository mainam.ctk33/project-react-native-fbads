import React, { Component } from 'react'
import { StyleSheet, TextInput } from 'react-native'
// import { Icon } from 'expo';
import Text from '@components/common/Text';
import Block from '@components/common/Block';
import Button from '@components/common/Button';
import themes from '@themes';
export default class Input extends Component {
  state = {
    toggleSecure: false,
  }
  renderLabel() {
    const { label, error } = this.props;
    return (
      <Block flex={false}>
        {label ? <Text gray2={!error} accent={error}>{label}</Text> : null}
      </Block>
    )
  }
  renderToggle() {
    const { secure, rightLabel } = this.props;
    const { toggleSecure } = this.state;
    return null;
    // if (!secure) return null;
    // return (
    //   <Button
    //     style={styles.toggle}
    //     onPress={() => this.setState({ toggleSecure: !toggleSecure })}
    //   >
    //     {
    //       rightLabel ? rightLabel :
    //         <Icon.Ionicons
    //           color={themes.getTheme().Colors.gray}
    //           size={themes.getTheme().Sizes.font * 1.35}
    //           name={!toggleSecure ? "md-eye" : "md-eye-off"}
    //       />
    //     }
    //   </Button>
    // );
  }
  renderRight() {
    const { rightLabel, rightStyle, onRightPress } = this.props;
    if (!rightLabel) return null;
    return (
      <Button
        style={[styles.toggle, rightStyle]}
        onPress={() => onRightPress && onRightPress()}
      >
        {rightLabel}
      </Button>
    );
  }
  render() {
    const {
      email,
      phone,
      number,
      secure,
      error,
      style,
      ...props
    } = this.props;
    const { toggleSecure } = this.state;
    const isSecure = toggleSecure ? false : secure;
    const inputStyles = [
      styles.input,
      error && { borderColor: themes.getTheme().Colors.accent },
      style,
    ];
    const inputType = email
      ? 'email-address' : number
      ? 'numeric' : phone
      ? 'phone-pad' : 'default';
    return (
      <Block flex={false} margin={[themes.getTheme().Sizes.base, 0]}>
        {this.renderLabel()}
        <TextInput
          style={inputStyles}
          secureTextEntry={isSecure}
          autoComplete="off"
          autoCapitalize="none"
          autoCorrect={false}
          keyboardType={inputType}
          {...props}
        />
        {this.renderToggle()}
        {this.renderRight()}
      </Block>
    )
  }
}
const styles = StyleSheet.create({
  input: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: themes.getTheme().Colors.black,
    borderRadius: themes.getTheme().Sizes.radius,
    fontSize: themes.getTheme().Sizes.font,
    fontWeight: '500',
    color: themes.getTheme().Colors.black,
    height: themes.getTheme().Sizes.base * 3,
  },
  toggle: {
    position: 'absolute',
    alignItems: 'flex-end',
    width: themes.getTheme().Sizes.base * 2,
    height: themes.getTheme().Sizes.base * 2,
    top: themes.getTheme().Sizes.base,
    right: 0,
  }
});