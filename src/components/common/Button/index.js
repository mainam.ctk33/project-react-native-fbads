import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import themes from '@themes';
class Button extends Component {
  render() {
    const {
      style,
      opacity,
      gradient,
      color,
      startColor,
      endColor,
      end,
      start,
      locations,
      shadow,
      children,
      ...props
    } = this.props;
    const buttonStyles = [
      styles.button,
      shadow && styles.shadow,
      color && styles[color], // predefined styles colors for backgroundColor
      color && !styles[color] && { backgroundColor: color }, // custom backgroundColor
      style,
    ];
    if (gradient) {
      return (
        <TouchableOpacity
          style={buttonStyles}
          activeOpacity={opacity}
          {...props}
        >
          <LinearGradient
            start={start}
            end={end}
            locations={locations}
            style={buttonStyles}
            colors={[startColor, endColor]}
          >
            {children}
          </LinearGradient>
        </TouchableOpacity>
      )
    }
    return (
      <TouchableOpacity
        style={buttonStyles}
        activeOpacity={opacity || 0.8}
        {...props}
      >
        {children}
      </TouchableOpacity>
    )
  }
}
Button.defaultProps = {
  startColor: themes.getTheme().Colors.primary,
  endColor: themes.getTheme().Colors.secondary,
  start: { x: 0, y: 0 },
  end: { x: 1, y: 1 },
  locations: [0.1, 0.9],
  opacity: 0.8,
  color: themes.getTheme().Colors.white,
}
export default Button;
const styles = StyleSheet.create({
  button: {
    borderRadius: themes.getTheme().Sizes.radius,
    height: themes.getTheme().Sizes.base * 3,
    justifyContent: 'center',
    marginVertical: themes.getTheme().Sizes.padding / 3,
  },
  shadow: {
    shadowColor: themes.getTheme().Colors.black,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 10,
  },
  accent: { backgroundColor: themes.getTheme().Colors.accent, },
  primary: { backgroundColor: themes.getTheme().Colors.primary, },
  secondary: { backgroundColor: themes.getTheme().Colors.secondary, },
  tertiary: { backgroundColor: themes.getTheme().Colors.tertiary, },
  black: { backgroundColor: themes.getTheme().Colors.black, },
  white: { backgroundColor: themes.getTheme().Colors.white, },
  gray: { backgroundColor: themes.getTheme().Colors.gray, },
  gray2: { backgroundColor: themes.getTheme().Colors.gray2, },
  gray3: { backgroundColor: themes.getTheme().Colors.gray3, },
  gray4: { backgroundColor: themes.getTheme().Colors.gray4, },
});