import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import Block from '@components/common/Block';
import themes from '@themes';
export default class Card extends Component {
  render() {
    const { color, style, children, ...props } = this.props;
    const cardStyles = [
      styles.card,
      style,
    ];
    return (
      <Block color={color || themes.getTheme().Colors.white} style={cardStyles} {...props}>
        {children}
      </Block>
    )
  }
}
export const styles = StyleSheet.create({
  card: {
    borderRadius: themes.getTheme().Sizes.radius,
    padding: themes.getTheme().Sizes.base + 4,
    marginBottom: themes.getTheme().Sizes.base,
  },
})