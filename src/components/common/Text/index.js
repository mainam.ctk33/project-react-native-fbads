import React, { Component } from "react";
import { Text, StyleSheet } from "react-native";
import themes from '@themes';
export default class Typography extends Component {
  render() {
    const {
      h1,
      h2,
      h3,
      title,
      body,
      caption,
      small,
      size,
      transform,
      align,
      // styling
      regular,
      bold,
      semibold,
      medium,
      weight,
      light,
      center,
      right,
      spacing, // letter-spacing
      height, // line-height
      // colors
      color,
      accent,
      primary,
      secondary,
      tertiary,
      black,
      white,
      gray,
      gray2,
      style,
      children,
      ...props
    } = this.props;
    const textStyles = [
      styles.text,
      h1 && styles.h1,
      h2 && styles.h2,
      h3 && styles.h3,
      title && styles.title,
      body && styles.body,
      caption && styles.caption,
      small && styles.small,
      size && { fontSize: size },
      transform && { textTransform: transform },
      align && { textAlign: align },
      height && { lineHeight: height },
      spacing && { letterSpacing: spacing },
      weight && { fontWeight: weight },
      regular && styles.regular,
      bold && styles.bold,
      semibold && styles.semibold,
      medium && styles.medium,
      light && styles.light,
      center && styles.center,
      right && styles.right,
      color && styles[color],
      color && !styles[color] && { color },
      // color shortcuts
      accent && styles.accent,
      primary && styles.primary,
      secondary && styles.secondary,
      tertiary && styles.tertiary,
      black && styles.black,
      white && styles.white,
      gray && styles.gray,
      gray2 && styles.gray2,
      style // rewrite predefined styles
    ];
    return (
      <Text style={textStyles} {...props}>
        {children}
      </Text>
    );
  }
}
const styles = StyleSheet.create({
  // default style
  text: {
    fontSize: themes.getTheme().Sizes.font,
    color: themes.getTheme().Colors.black
  },
  // variations
  regular: {
    fontWeight: "normal",
  },
  bold: {
    fontWeight: "bold",
  },
  semibold: {
    fontWeight: "500",
  },
  medium: {
    fontWeight: "500",
  },
  light: {
    fontWeight: "200",
  },
  // position
  center: { textAlign: "center" },
  right: { textAlign: "right" },
  // colors
  accent: { color: themes.getTheme().Colors.accent },
  primary: { color: themes.getTheme().Colors.primary },
  secondary: { color: themes.getTheme().Colors.secondary },
  tertiary: { color: themes.getTheme().Colors.tertiary },
  black: { color: themes.getTheme().Colors.black },
  white: { color: themes.getTheme().Colors.white },
  gray: { color: themes.getTheme().Colors.gray },
  gray2: { color: themes.getTheme().Colors.gray2 },
  // fonts
  h1: themes.getTheme().Fonts.style.h1,
  h2: themes.getTheme().Fonts.style.h2,
  h3: themes.getTheme().Fonts.style.h3,
  title: themes.getTheme().Fonts.style.title,
  body: themes.getTheme().Fonts.style.body,
  caption: themes.getTheme().Fonts.style.caption,
  small: themes.getTheme().Fonts.style.small
});