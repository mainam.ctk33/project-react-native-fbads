import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Text,
  TouchableOpacity,
  View,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
} from "react-native";
import ReactNativeModal from "react-native-modal";
import { connect } from "react-redux";

import styles from "./index.style";
class ActionSheet extends Component {
  static propTypes = {
    onBackdropPress: PropTypes.func,
    onModalHide: PropTypes.func,
  };

  handleOnPressAction = (index) => () => {
    if (this.props.onActionPress) {
      this.props.onActionPress(index);
    }
  };

  render() {
    const { isVisible } = this.props;

    let actions = this.props.actions || [];
    if (!actions && !actions.length) {
      return null;
    }
    let dismissAction = this.props.dismissAction;
    if (dismissAction === undefined) {
      dismissAction = -1;
    }
    return (
      <ReactNativeModal
        onBackdropPress={this.props.onBackdropPress}
        isVisible={isVisible}
        style={[styles.contentContainer]}
        onModalHide={this.props.onModalHide}
        backdropOpacity={0.4}
      >
        <KeyboardAvoidingView
          enabled={Platform.OS == "ios"}
          keyboardVerticalOffset={Platform.OS == "android" ? -500 : 0}
          behavior="padding"
          style={{
            width: Math.min(this.props.theme.Metrics.screenWidth, 500),
            alignSelf: "center",
          }}
        >
          {this.props.children && (
            <View style={[styles.datepickerContainer]}>
              <View
                onStartShouldSetResponderCapture={this._handleUserTouchInit}
              >
                {this.props.children}
              </View>
            </View>
          )}

          <View style={styles.actionsArea}>
            {actions.map((item, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  underlayColor="#ebebeb"
                  style={[
                    styles.actionButton,
                    index == 0
                      ? {
                          borderTopWidth: 0,
                        }
                      : {},
                  ]}
                  onPress={this.handleOnPressAction(index)}
                >
                  <Text
                    style={[
                      styles.actionButtonText,
                      index === dismissAction ? styles.dismissAction : {},
                    ]}
                  >
                    {item}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </View>
        </KeyboardAvoidingView>
      </ReactNativeModal>
    );
  }
}

export default connect(
  (state) => {
    return {
      theme: state.theme.theme,
    };
  },
  {}
)(ActionSheet);
