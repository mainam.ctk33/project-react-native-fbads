import React, { Component } from "react";
import {
  Text,
  StatusBar,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
  StyleSheet,
  View,
  Platform,
  Linking,
} from "react-native";
import PropTypes from "prop-types";
const DEVICE_WIDTH = Dimensions.get("window").width;
import Activity from "mainam-react-native-activity-panel";
import { connect } from "react-redux";
import ScaledImage from "mainam-react-native-scaleimage";
import { SafeAreaView } from "react-navigation";
import { Card } from "native-base";
import * as navigationUtils from "@utils/navigation-utils";

class ActivityPanel extends Component {
  constructor(props) {
    super(props);
    let paddingTop = 0;
    if (this.props.paddingTop >= 0) {
      paddingTop = this.props.paddingTop;
    }
    this.state = {
      paddingTop: paddingTop,
      zoomMode: false,
    };
  }
  backPress = () => {
    navigationUtils.pop();
  };

  getActionbar() {
    return null;
  }

  getLoadingView() {
    return (
      <View style={styles.containerLoading}>
        <ActivityIndicator size={"large"} color={"#02C39A"} />
      </View>
    );
  }
  getContent = () => {
    let content = this.props.children;
    return (
      <Activity
        icBack={this.props.theme.Images.icBack}
        iosBarStyle={"light-content"}
        {...this.props}
        containerStyle={[styles.container, this.props.containerStyle]}
        style={{ backgroundColor: "#1D93D1" }}
        actionbar={
          this.props.actionbar
            ? this.props.actionbar
            : this.getActionbar.bind(this)
        }
        loadingView={this.getLoadingView()}
      >
        {this.props.transparent ? (
          this.props.useCard ? (
            <View
              style={[
                { flex: 1, paddingHorizontal: 10 },
                this.props.containerStyle,
              ]}
            >
              <Card
                style={[
                  {
                    flex: 1,
                    paddingBottom: 0,
                    marginBottom: -10,
                    borderRadius: 10,
                  },
                  this.props.cardStyle,
                ]}
              >
                {this.props.children}
              </Card>
            </View>
          ) : (
            content
          )
        ) : (
          <View
            style={[
              { flex: 1, backgroundColor: "#FFF" },
              this.props.containerStyle,
            ]}
          >
            {this.props.useCard ? (
              <Card
                style={[
                  {
                    flex: 1,
                    paddingBottom: 0,
                    marginBottom: -10,
                    borderRadius: 10,
                  },
                  this.props.cardStyle,
                ]}
              >
                {content}
              </Card>
            ) : (
              content
            )}
          </View>
        )}
      </Activity>
    );
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar translucent barStyle="light-content" />

        {/* <ScaledImage source={this.props.theme.Images.bgHeader} width={DEVICE_WIDTH} style={{ position: "absolute", top: 0, left: 0, right: 0 }} /> */}
        <View style={{ height: 1, position: "relative", flex: 1 }}>
          <View
            style={{
              height: 200,
              backgroundColor: this.props.theme.Colors.primaryColor,
              position: "absolute",
              top: 0,
              left: 0,
              right: 0,
            }}
          />
          {/* {
            (this.props.showFullScreen && (this.props.showBackgroundHeader || this.props.showBackgroundHeader === undefined)) ?
              this.props.backgroundHeader ?
                <ScaledImage source={this.props.backgroundHeader} width={DEVICE_WIDTH} style={{ position: "absolute", top: 0, left: 0, right: 0 }} />
                :
                <ScaledImage source={this.props.theme.Images.bgHeader} width={DEVICE_WIDTH} style={{ position: "absolute", top: 0, left: 0, right: 0 }} />
              :
              null
          } */}
          {this.showBackground === false ? null : (
            <ScaledImage
              source={this.props.theme.Images.bgScreen}
              height={200}
              width={DEVICE_WIDTH}
              style={styles.imageBackground}
            />
          )}
          {this.props.showFullScreen ? (
            this.getContent()
          ) : (
            <SafeAreaView
              style={{
                flex: 1,
                paddingTop:
                  Platform.OS === "android" &&
                  (!this.props.hideStatusBar ||
                    this.props.hideStatusBar === undefined)
                    ? StatusBar.currentHeight
                    : 0,
              }}
              forceInset={Platform.OS === "android" && { vertical: "never" }}
            >
              {this.getContent()}
            </SafeAreaView>
          )}
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  imageBackground: {
    position: "absolute",
    bottom: 10,
    right: 10,
  },
  container: {
    backgroundColor: "#f7f9fb",
  },
  containerLoading: {
    position: "absolute",
    backgroundColor: "#bfeaff94",
    flex: 1,
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
  },
  actionbarStyle: {
    backgroundColor: "#27c8ad",
    borderBottomWidth: 0,
  },
  titleStyle: {
    color: "#FFF",
    marginLeft: 10,
    fontSize: 15,
  },
});
export default connect(
  (state) => {
    return {
      theme: state.theme.theme,
      auth: state.auth.auth,
    };
  },
  {},
  null,
  { forwardRef: true }
)(ActivityPanel);
