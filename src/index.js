/* eslint-disable prettier/prettier */
import React, { Component } from "react";
import AppContainer from "@navigators/AppNavigator";
import codePush from "react-native-code-push";
let codePushOptions = { checkFrequency: codePush.CheckFrequency.MANUAL };
// let codePushOptions = {installMode: codePush.InstallMode.IMMEDIATE };
import { Text, TextInput, Animated, StyleSheet } from "react-native";
import codePushUtils from "@utils/codepush-utils";
Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;
TextInput.defaultProps = TextInput.defaultProps || {};
TextInput.defaultProps.allowFontScaling = false;
Animated.Text.defaultProps = TextInput.defaultProps || {};
Animated.Text.defaultProps.allowFontScaling = false;

import FlashMessage from "react-native-flash-message";
import Provider from "@redux/Provider";
import fonts from "@resources/fonts";
Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;
TextInput.defaultProps = TextInput.defaultProps || {};
TextInput.defaultProps.allowFontScaling = false;
TextInput.defaultProps.placeholderTextColor = "#BBB";
Animated.Text.defaultProps = TextInput.defaultProps || {};
Animated.Text.defaultProps.allowFontScaling = false;

class Kernel extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.SetDefaultText();
  }

  componentDidMount() {
    codePushUtils.checkupDate(true);
  }
  SetDefaultText = () => {
    let components = [Text, TextInput];
    for (let i = 0; i < components.length; i++) {
      const TextRender = components[i].render;
      components[i].render = function(...args) {
        let origin = TextRender.call(this, ...args);
        if (
          origin.props &&
          origin.props.style &&
          origin.props.style.fontWeight
        ) {
          fontFamily = fonts[`${origin.props.style.fontWeight}`];
          return React.cloneElement(origin, {
            style: StyleSheet.flatten([
              origin.props.style,
              {
                fontFamily: fonts[`${origin.props.style.fontWeight}`],
                fontWeight: undefined,
              },
            ]),
          });
        }
        return React.cloneElement(origin, {
          style: StyleSheet.flatten([
            origin.props.style,
            { fontFamily: fonts["500"] },
          ]),
        });
      };
    }
  };
  render() {
    return (
      <>
        <Provider>
          <AppContainer />
        </Provider>
        <FlashMessage
          style={{ marginTop: 50 }}
          floating={true}
          position="top"
        />
      </>
    );
  }
}
// export default Kernel;
export default codePush(codePushOptions)(Kernel);
