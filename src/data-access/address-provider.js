import client from "@utils/client-utils";
import strings from "@strings";
module.exports = {
  getCity() {
    return client.requestApi("get", strings.api.address.city + "?page=0&active=true", {});
  },
  getDetailCity(id) {
    return client.requestApi("get", strings.api.address.city + "/" + id, {});
  },
  getDistrict(dmTinhThanhPhoId) {
    return client.requestApi(
      "get",
      strings.api.address.district +
        "?page=0&active=true&tinhThanhPhoId=" +
        dmTinhThanhPhoId,
      {}
    );
  },
  getDetailDistrict(id) {
    return client.requestApi(
      "get",
      strings.api.address.district + "/" + id,
      {}
    );
  },
  getTown(dmQuanHuyenId) {
    return client.requestApi(
      "get",
      strings.api.address.town + "?page=0&active=true&quanHuyenId=" + dmQuanHuyenId,
      {}
    );
  },
  getDetailTown(id) {
    return client.requestApi("get", strings.api.address.town + "/" + id, {});
  },
  getCountry() {
    return client.requestApi(
      "get",
      strings.api.address.country + "?page=0&active=true",
      {}
    );
  },
  getDetailCountry(id) {
    return client.requestApi("get", strings.api.address.country + "/" + id, {});
  },
};
