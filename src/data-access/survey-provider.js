import client from '@utils/client-utils';
import strings from '@strings';
module.exports = {
    submit(data) {
        return client.requestApi("post", strings.api.survey.submit, data);
    },
    update(id, data) {
        return client.requestApi("put", strings.api.survey.submit + "/" + id, data);
    }
}