import client from "@utils/client-utils";
import strings from "@strings";
module.exports = {
  getAllDonVi() {
    return client.requestApi("get", strings.api.category.donvi + "?page=0&active=true", {});
  },
  getAllKhuVuc() {
    return client.requestApi(
      "get",
      strings.api.category.khuvuc + "?page=0&active=true",
      {}
    );
  },
  getAllNgheNghiep() {
    return client.requestApi(
      "get",
      strings.api.category.nghenghiep + "?page=0&active=true",
      {}
    );
  },
  getDetailNgheNghiep(id) {
    return client.requestApi(
      "get",
      strings.api.category.nghenghiep + "/" + id,
      {}
    );
  },
  getAllKhoa() {
    return client.requestApi("get", strings.api.category.khoa + "?page=0&active=true", {});
  },
};
