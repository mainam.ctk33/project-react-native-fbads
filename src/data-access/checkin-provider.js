import client from '@utils/client-utils';
import strings from '@strings';
module.exports = {
    searchInfo(ma, page, size) {
        return client.requestApi("get", strings.api.checkin.search + '?timKiem=' + ma);
    },
    searchCheckin(ma, page, size) {
        return client.requestApi("get", strings.api.checkin.search + '?page=' + page + '&ma=' + ma + '&size=' + size + '&sort=id,desc');
    },
    checkin(data) {
        return client.requestApi("post", strings.api.checkin.checkin, data);
    },
    checkout(id) {
        return client.requestApi("put", strings.api.checkin.checkout + "/" + id, {});
    },
    updateCheckin(id, data) {
        return client.requestApi("put", strings.api.checkin.checkin + "/" + id, data);
    }
}

