import client from '@utils/client-utils';
import strings from '@strings';
module.exports = {
    uploadImage(file) {
        return client.uploadFile(strings.api.file.upload_avatar, file);
    },
    uploadMultiImage(files) {
        return client.uploadFile(strings.api.file.upload_cardId, files);
    }
}

