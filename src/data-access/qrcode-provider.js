import client from '@utils/client-utils';
import strings from '@strings';
module.exports = {
    search(qrcode) {
        return client.requestApi("post", strings.api.tthanhchinh.search, { qr: qrcode });
    }
}