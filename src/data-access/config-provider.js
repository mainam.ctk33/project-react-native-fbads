import client from "@utils/client-utils";
import strings from "@strings";
module.exports = {
  getValue(configs, configName, defaultValue) {
    try {
      let config = configs.find((x) => x.maThietLap == configName) || {};
      if (config) return config.giaTri;
      return defaultValue;
    } catch (error) {
      return defaultValue;
    }
  },
  getConfig(donViId) {
    return client.requestApi(
      "get",
      strings.api.config.get_config + "?donViId=" + donViId
    );
  },
  setConfig(configs = []) {
    return client.requestApi("put", strings.api.config.set_config, configs);
  },
};
