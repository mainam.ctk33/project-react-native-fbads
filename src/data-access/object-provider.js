import client from "@utils/client-utils";
import strings from "@strings";
module.exports = {
  getObject(donViId) {
    return client.requestApi(
      "get",
      strings.api.object.get_all + "?page=0&donViId=" + donViId + "&sort=id",
      {}
    );
  },
};
