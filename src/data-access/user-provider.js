import client from "@utils/client-utils";
import strings from "@strings";
import hmacSHA256 from "crypto-js/hmac-sha256";
export default {
  login(username, password) {
    const signature = hmacSHA256(
      username,
      "4IJ6SaBigVJt0My4pZcOiMmP6bequQio"
    ).toString();
    return client.requestApi("post", strings.api.user.login, {
      username,
      password,
      signature,
    });
  },
};
