import client from '@utils/client-utils';
import string from "@strings";
import dateUtils from 'mainam-react-native-date-utils';
import datacacheProvider from '@data-access/datacache-provider';
module.exports = {
    tongKhamBenh(fromDate, toDate, requestApi) {
        return new Promise(async (resolve, reject) => {

            await client.requestApi("get", `${string.api.dashboard.tong_kham_benh}?tuNgay=${fromDate.format("yyyy-MM-dd")}&denNgay=${toDate.format("yyyy-MM-dd")}`, {})
                .then(s => {
                    if (s.code == 0) {
                        datacacheProvider.save(
                            "",
                            "tongKhamBenh",
                            s
                        );
                        resolve(s);
                    }
                    reject(s);
                })
            // if (!requestApi) {
            //     let data = await datacacheProvider.read(
            //         "",
            //         "tongKhamBenh",
            //         null
            //     );
            //     if (data && data.code == 0) {
            //         this.tongKhamBenh(fromDate, toDate, true);
            //         resolve(data);
            //     } else {
            //         this.tongKhamBenh(fromDate, toDate, true)
            //             .then(s => {
            //                 resolve(s);
            //             })
            //             .catch(e => {
            //                 reject(e);
            //             });
            //     }
            // } else {
            //     client.requestApi("get", `${string.api.dashboard.tong_kham_benh}?tuNgay=${fromDate.format("yyyy-MM-dd")}&denNgay=${toDate.format("yyyy-MM-dd")}`, {})
            //         .then(s => {
            //             if (s.code == 0) {
            //                 datacacheProvider.save(
            //                     "",
            //                     "tongKhamBenh",
            //                     s
            //                 );
            //                 resolve(s);
            //             }
            //             reject(s);
            //         })

            // }
        })
    },
    bnDangDieuTri(ngay, requestApi) {
        return new Promise(async (resolve, reject) => {


            await client.requestApi("get", `${string.api.dashboard.tong_dieu_tri}?ngay=${ngay.format("yyyy-MM-dd")}`, {})
                .then(s => {
                    if (s.code == 0) {
                        datacacheProvider.save(
                            "",
                            "bnDangDieuTri",
                            s
                        );
                        resolve(s);
                    }
                    reject(s);
                })
            // if (!requestApi) {
            //     let data = await datacacheProvider.read(
            //         "",
            //         "bnDangDieuTri",
            //         null
            //     );
            //     if (data && data.code == 0) {
            //         this.bnDangDieuTri(ngay, true);
            //         resolve(data);
            //     } else {
            //         this.bnDangDieuTri(ngay, true)
            //             .then(s => {
            //                 resolve(s);
            //             })
            //             .catch(e => {
            //                 reject(e);
            //             });
            //     }
            // } else {
            //     client.requestApi("get", `${string.api.dashboard.tong_dieu_tri}?ngay=${ngay.format("yyyy-MM-dd")}`, {})
            //         .then(s => {
            //             if (s.code == 0) {
            //                 datacacheProvider.save(
            //                     "",
            //                     "bnDangDieuTri",
            //                     s
            //                 );
            //                 resolve(s);
            //             }
            //             reject(s);
            //         })

            // }
        })
    },
    bnPhauThuat(ngay, requestApi) {
        return new Promise(async (resolve, reject) => {

            await client.requestApi("get", `${string.api.dashboard.tong_phau_thuat}?ngay=${ngay.format("yyyy-MM-dd")}`, {})
                .then(s => {
                    if (s.code == 0) {
                        datacacheProvider.save(
                            "",
                            "bnPhauThuat",
                            s
                        );
                        resolve(s);
                    }
                    reject(s);
                })

            // if (!requestApi) {
            //     let data = await datacacheProvider.read(
            //         "",
            //         "bnPhauThuat",
            //         null
            //     );
            //     if (data && data.code == 0) {
            //         this.bnPhauThuat(ngay, true);
            //         resolve(data);
            //     } else {
            //         this.bnPhauThuat(ngay, true)
            //             .then(s => {
            //                 resolve(s);
            //             })
            //             .catch(e => {
            //                 reject(e);
            //             });
            //     }
            // } else {
            //     client.requestApi("get", `${string.api.dashboard.tong_phau_thuat}?ngay=${ngay.format("yyyy-MM-dd")}`, {})
            //         .then(s => {
            //             if (s.code == 0) {
            //                 datacacheProvider.save(
            //                     "",
            //                     "bnPhauThuat",
            //                     s
            //                 );
            //                 resolve(s);
            //             }
            //             reject(s);
            //         })

            // }
        })
    },
    tongThuKhamBenh(fromDate, toDate, requestApi) {
        return new Promise(async (resolve, reject) => {

            await client.requestApi("get", `${string.api.dashboard.tong_thu_kham_benh}?tuNgay=${fromDate.format("yyyy-MM-dd")}&denNgay=${toDate.format("yyyy-MM-dd")}`, {})
                .then(s => {
                    if (s.code == 0) {
                        datacacheProvider.save(
                            "",
                            "tongThuKhamBenh",
                            s
                        );
                        resolve(s);
                    }
                    reject(s);
                })
            // if (!requestApi) {
            //     let data = await datacacheProvider.read(
            //         "",
            //         "tongThuKhamBenh",
            //         null
            //     );
            //     if (data && data.code == 0) {
            //         this.tongThuKhamBenh(fromDate, toDate, true);
            //         resolve(data);
            //     } else {
            //         this.tongThuKhamBenh(fromDate, toDate, true)
            //             .then(s => {
            //                 resolve(s);
            //             })
            //             .catch(e => {
            //                 reject(e);
            //             });
            //     }
            // } else {
            //     client.requestApi("get", `${string.api.dashboard.tong_thu_kham_benh}?tuNgay=${fromDate.format("yyyy-MM-dd")}&denNgay=${toDate.format("yyyy-MM-dd")}`, {})
            //         .then(s => {
            //             if (s.code == 0) {
            //                 datacacheProvider.save(
            //                     "",
            //                     "tongThuKhamBenh",
            //                     s
            //                 );
            //                 resolve(s);
            //             }
            //             reject(s);
            //         })

            // }
        })
    },
    tongThuVienPhiNhieuNgay(fromDate, toDate, requestApi) {
        return new Promise(async (resolve, reject) => {

            client.requestApi("get", `${string.api.dashboard.tong_thu_vien_phi_nhieu_ngay}?tuNgay=${fromDate.format("yyyy-MM-dd")}&denNgay=${toDate.format("yyyy-MM-dd")}`, {})
                .then(s => {
                    if (s.code == 0) {
                        datacacheProvider.save(
                            "",
                            "tongThuVienPhiNhieuNgay",
                            s
                        );
                        resolve(s);
                    }
                    reject(s);
                })


            // if (!requestApi) {
            //     let data = await datacacheProvider.read(
            //         "",
            //         "tongThuVienPhiNhieuNgay",
            //         null
            //     );
            //     if (data && data.code == 0) {
            //         this.tongThuVienPhiNhieuNgay(fromDate, toDate, true);
            //         resolve(data);
            //     } else {
            //         this.tongThuVienPhiNhieuNgay(fromDate, toDate, true)
            //             .then(s => {
            //                 resolve(s);
            //             })
            //             .catch(e => {
            //                 reject(e);
            //             });
            //     }
            // } else {
            //     client.requestApi("get", `${string.api.dashboard.tong_thu_vien_phi_nhieu_ngay}?tuNgay=${fromDate.format("yyyy-MM-dd")}&denNgay=${toDate.format("yyyy-MM-dd")}`, {})
            //         .then(s => {
            //             if (s.code == 0) {
            //                 datacacheProvider.save(
            //                     "",
            //                     "tongThuVienPhiNhieuNgay",
            //                     s
            //                 );
            //                 resolve(s);
            //             }
            //             reject(s);
            //         })

            // }
        })
    },
    tongThuVienPhi(fromDate,toDate, requestApi) {


        return new Promise(async (resolve, reject) => {

            client.requestApi("get", `${string.api.dashboard.tong_thu_vien_phi}?tuNgay=${fromDate.format("yyyy-MM-dd")}&denNgay=${toDate.format("yyyy-MM-dd")}`, {})
                .then(s => {
                    if (s.code == 0) {
                        datacacheProvider.save(
                            "",
                            "tongThuVienPhi",
                            s
                        );
                        resolve(s);
                    }
                    reject(s);
                })
            // if (!requestApi) {
            //     let data = await datacacheProvider.read(
            //         "",
            //         "tongThuVienPhi",
            //         null
            //     );
            //     if (data && data.code == 0) {
            //         this.tongThuVienPhi(ngay, true);
            //         resolve(data);
            //     } else {
            //         this.tongThuVienPhi(ngay, true)
            //             .then(s => {
            //                 resolve(s);
            //             })
            //             .catch(e => {
            //                 reject(e);
            //             });
            //     }
            // } else {
            //     client.requestApi("get", `${string.api.dashboard.tong_thu_vien_phi}?ngay=${ngay.format("yyyy-MM-dd")}`, {})
            //         .then(s => {
            //             if (s.code == 0) {
            //                 datacacheProvider.save(
            //                     "",
            //                     "tongThuVienPhi",
            //                     s
            //                 );
            //                 resolve(s);
            //             }
            //             reject(s);
            //         })

            // }
        })

        // return client.requestApi("get", `${string.api.dashboard.tong_thu_vien_phi}?ngay=${ngay.format("yyyy-MM-dd")}`, {})
        return client.requestApi("get", `${string.api.dashboard.tong_thu_vien_phi}?ngay=2019-10-04`, {})
    },
    soLuongKhamNgayGio(ngay, requestApi) {
        return new Promise(async (resolve, reject) => {

            // client.requestApi("get", `${string.api.dashboard.so_luong_kham}?ngay=${ngay.format("yyyy-MM-dd")}`, {})
            //     .then(s => {
            //         if (s.code == 0) {
            //             datacacheProvider.save(
            //                 "",
            //                 "soLuongKhamNgayGio",
            //                 s
            //             );
            //             resolve(s);
            //         }
            //         reject(s);
            //     })
            if (!requestApi) {
                let data = await datacacheProvider.read(
                    "",
                    "soLuongKhamNgayGio",
                    null
                );
                if (data && data.code == 0) {
                    this.soLuongKhamNgayGio(ngay, true);
                    resolve(data);
                } else {
                    this.soLuongKhamNgayGio(ngay, true)
                        .then(s => {
                            resolve(s);
                        })
                        .catch(e => {
                            reject(e);
                        });
                }
            } else {
                client.requestApi("get", `${string.api.dashboard.so_luong_kham}?ngay=${ngay.format("yyyy-MM-dd")}`, {})
                    .then(s => {
                        if (s.code == 0) {
                            datacacheProvider.save(
                                "",
                                "soLuongKhamNgayGio",
                                s
                            );
                            resolve(s);
                        }
                        reject(s);
                    })

            }
        })
    },
    gioKhamBenh(ngay, requestApi) {
        return new Promise(async (resolve, reject) => {

            await client.requestApi("get", `${string.api.dashboard.gio_kham_benh}?ngay=${ngay.format("yyyy-MM-dd")}`, {})
                .then(s => {
                    if (s.code == 0) {
                        datacacheProvider.save(
                            "",
                            "gioKhamBenh",
                            s
                        );
                        resolve(s);
                    }
                    reject(s);
                })


            // if (!requestApi) {
            //     let data = await datacacheProvider.read(
            //         "",
            //         "gioKhamBenh",
            //         null
            //     );
            //     if (data && data.code == 0) {
            //         this.gioKhamBenh(ngay, true);
            //         resolve(data);
            //     } else {
            //         this.gioKhamBenh(ngay, true)
            //             .then(s => {
            //                 resolve(s);
            //             })
            //             .catch(e => {
            //                 reject(e);
            //             });
            //     }
            // } else {
            //     client.requestApi("get", `${string.api.dashboard.gio_kham_benh}?ngay=${ngay.format("yyyy-MM-dd")}`, {})
            //         .then(s => {
            //             if (s.code == 0) {
            //                 datacacheProvider.save(
            //                     "",
            //                     "gioKhamBenh",
            //                     s
            //                 );
            //                 resolve(s);
            //             }
            //             reject(s);
            //         })

            // }
        })
    },
    vaoVienNoiTru(ngay, requestApi) {
        return new Promise(async (resolve, reject) => {


            await client.requestApi("get", `${string.api.dashboard.noi_tru_vao_vien}?ngay=${ngay.format("yyyy-MM-dd")}`, {})
                .then(s => {
                    if (s.code == 0) {
                        datacacheProvider.save(
                            "",
                            "vaoVienNoiTru",
                            s
                        );
                        resolve(s);
                    }
                    reject(s);
                })
            // if (!requestApi) {
            //     let data = await datacacheProvider.read(
            //         "",
            //         "vaoVienNoiTru",
            //         null
            //     );
            //     if (data && data.code == 0) {
            //         this.vaoVienNoiTru(ngay, true);
            //         resolve(data);
            //     } else {
            //         this.vaoVienNoiTru(ngay, true)
            //             .then(s => {
            //                 resolve(s);
            //             })
            //             .catch(e => {
            //                 reject(e);
            //             });
            //     }
            // } else {
            //     client.requestApi("get", `${string.api.dashboard.noi_tru_vao_vien}?ngay=${ngay.format("yyyy-MM-dd")}`, {})
            //         .then(s => {
            //             if (s.code == 0) {
            //                 datacacheProvider.save(
            //                     "",
            //                     "vaoVienNoiTru",
            //                     s
            //                 );
            //                 resolve(s);
            //             }
            //             reject(s);
            //         })

            // }
        })
    },
    vaoVienNgoaiTru(ngay, requestApi) {
        return new Promise(async (resolve, reject) => {


            await client.requestApi("get", `${string.api.dashboard.ngoai_tru_vao_vien}?ngay=${ngay.format("yyyy-MM-dd")}`, {})
                .then(s => {
                    if (s.code == 0) {
                        datacacheProvider.save(
                            "",
                            "vaoVienNgoaiTru",
                            s
                        );
                        resolve(s);
                    }
                    reject(s);
                })
            // if (!requestApi) {
            //     let data = await datacacheProvider.read(
            //         "",
            //         "vaoVienNgoaiTru",
            //         null
            //     );
            //     if (data && data.code == 0) {
            //         this.vaoVienNgoaiTru(ngay, true);
            //         resolve(data);
            //     } else {
            //         this.vaoVienNgoaiTru(ngay, true)
            //             .then(s => {
            //                 resolve(s);
            //             })
            //             .catch(e => {
            //                 reject(e);
            //             });
            //     }
            // } else {
            //     client.requestApi("get", `${string.api.dashboard.ngoai_tru_vao_vien}?ngay=${ngay.format("yyyy-MM-dd")}`, {})
            //         .then(s => {
            //             if (s.code == 0) {
            //                 datacacheProvider.save(
            //                     "",
            //                     "vaoVienNgoaiTru",
            //                     s
            //                 );
            //                 resolve(s);
            //             }
            //             reject(s);
            //         })

            // }
        })
    },
    raVienNoiTru(ngay, requestApi) {
        return new Promise(async (resolve, reject) => {


            await client.requestApi("get", `${string.api.dashboard.noi_tru_ra_vien}?ngay=${ngay.format("yyyy-MM-dd")}`, {})
                .then(s => {
                    if (s.code == 0) {
                        datacacheProvider.save(
                            "",
                            "raVienNoiTru",
                            s
                        );
                        resolve(s);
                    }
                    reject(s);
                })
            // if (!requestApi) {
            //     let data = await datacacheProvider.read(
            //         "",
            //         "raVienNoiTru",
            //         null
            //     );
            //     if (data && data.code == 0) {
            //         this.raVienNoiTru(ngay, true);
            //         resolve(data);
            //     } else {
            //         this.raVienNoiTru(ngay, true)
            //             .then(s => {
            //                 resolve(s);
            //             })
            //             .catch(e => {
            //                 reject(e);
            //             });
            //     }
            // } else {
            //     client.requestApi("get", `${string.api.dashboard.noi_tru_ra_vien}?ngay=${ngay.format("yyyy-MM-dd")}`, {})
            //         .then(s => {
            //             if (s.code == 0) {
            //                 datacacheProvider.save(
            //                     "",
            //                     "raVienNoiTru",
            //                     s
            //                 );
            //                 resolve(s);
            //             }
            //             reject(s);
            //         })

            // }
        })
    },
    raVienNgoaiTru(ngay, requestApi) {
        return new Promise(async (resolve, reject) => {
            await client.requestApi("get", `${string.api.dashboard.ngoai_tru_ra_vien}?ngay=${ngay.format("yyyy-MM-dd")}`, {})
                .then(s => {
                    if (s.code == 0) {
                        datacacheProvider.save(
                            "",
                            "raVienNgoaiTru",
                            s
                        );
                        resolve(s);
                    }
                    reject(s);
                })
            // if (!requestApi) {
            //     let data = await datacacheProvider.read(
            //         "",
            //         "raVienNgoaiTru",
            //         null
            //     );
            //     if (data && data.code == 0) {
            //         this.raVienNgoaiTru(ngay, true);
            //         resolve(data);
            //     } else {
            //         this.raVienNgoaiTru(ngay, true)
            //             .then(s => {
            //                 resolve(s);
            //             })
            //             .catch(e => {
            //                 reject(e);
            //             });
            //     }
            // } else {
            //     client.requestApi("get", `${string.api.dashboard.ngoai_tru_ra_vien}?ngay=${ngay.format("yyyy-MM-dd")}`, {})
            //         .then(s => {
            //             if (s.code == 0) {
            //                 datacacheProvider.save(
            //                     "",
            //                     "raVienNgoaiTru",
            //                     s
            //                 );
            //                 resolve(s);
            //             }
            //             reject(s);
            //         })

            // }
        })
    },
}