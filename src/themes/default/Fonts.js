import size from './Sizes';

const type = {
  base: 'Avenir-Book',
  bold: 'Avenir-Black',
  italic: 'PlayfairDisplay-Italic',
  emphasis: 'HelveticaNeue-Italic'
}


const style = {
  h1: {
    fontFamily: type.base,
    fontSize: size.h1
  },
  h2: {
    fontWeight: 'bold',
    fontSize: size.h2
  },
  h3: {
    fontFamily: type.emphasis,
    fontSize: size.h3
  },
  h4: {
    fontFamily: type.base,
    fontSize: size.h4
  },
  h5: {
    fontFamily: type.base,
    fontSize: size.h5
  },
  h6: {
    fontFamily: type.emphasis,
    fontSize: size.h6
  },
  normal: {
    fontFamily: type.base,
    fontSize: size.regular,
    backgroundColor: 'transparent'
  },
  bold: {
    fontFamily: type.bold,
    fontSize: size.regular,
    backgroundColor: 'transparent',
    fontWeight: 'bold'
  },
  description: {
    fontFamily: type.base,
    fontSize: size.medium,
    backgroundColor: 'transparent'
  },  
  header: {
    fontSize: size.header
  },
  title: {
    fontSize: size.title
  },
  body: {
    fontSize: size.body
  },
  caption: {
    fontSize: size.caption
  },
}

export default {
  type,
  size,
  style
}
