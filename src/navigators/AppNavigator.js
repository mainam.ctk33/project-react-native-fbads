import * as React from "react";
import { StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import SplashScreen from "@containers/SplashScreen";
import HomeScreen from "@containers/HomeScreen";
import Facebook from "@containers/Facebook";

import { navigationRef, isMountedRef } from "@utils/navigation-utils";

const Stack = createStackNavigator();
const Screen = Stack.Screen;

export default function App() {
  React.useEffect(() => {
    isMountedRef.current = true;
    return () => (isMountedRef.current = false);
  }, []);
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        headerMode="none"
        mode="modal"
        screenOptions={{
          headerTintColor: "white",
          headerStyle: { backgroundColor: "tomato" },
        }}
      >
        <Stack.Screen name="splash" component={SplashScreen} />
        <Stack.Screen name="facebook" component={Facebook} />
        <Stack.Screen name="home" component={HomeScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  drawer: {
    backgroundColor: "#0D2745",
  },
});

// import {yar
//   createDrawerNavigator,
//   DrawerItems,
//   createBottomTabNavigator,
//   TabBarBottom,
//   createStackNavigator,
//   createAppContainer,
//   BottomTabBar,
// } from 'react-navigation';
// import {
//   fromLeft,
//   zoomIn,
//   zoomOut,
//   fromRight,
// } from 'react-navigation-transitions';
// import SplashScreen from '@containers/SplashScreen';
// import LoginScreen from '@containers/auth/LoginScreen';
// import NewsScreen from '@containers/home/news/NewsScreen';
// import DetailNewsScreen from '@containers/home/news/DetailNewsScreen';
// import DayOffScreen from '@containers/home/dayoff/DayOffScreen';
// import RegisterDayOffScreen from '@containers/home/dayoff/RegisterDayOffScreen';
// import OverTimeScreen from '@containers/home/overtime/OverTimeScreen';
// import RegisterOTScreen from '@containers/home/overtime/RegisterOTScreen';
// import ApproveOverTimeScreen from '@containers/home/overtime/ApproveOverTimeScreen';
// import AccountScreen from '@containers/home/account/AccountScreen';
// import RuleScreen from '@containers/rules/RuleScreen';
// import AllowanceMgrScreen from '@containers/allowance/AllowanceMgrScreen';
// import EmployeeScreen from '@containers/employees/EmployeeScreen';
// import MyProfileScreen from '@containers/account/MyProfileScreen';
// import ChangePasswordScreen from '@containers/account/ChangePasswordScreen';
// import { Icon } from 'native-base';
// import { CheckinButton } from '@components/checkin/CheckinButton';
// import CheckinScreen from '@containers/checkin/CheckinScreen';
// const handleCustomTransition = ({ scenes }) => {
//   return fromRight();
// };

// const TabBarComponent = props => <BottomTabBar {...props} />
// const TabNavigatorComponent = createBottomTabNavigator(
//   {
//     home: {
//       screen: NewsScreen,
//       navigationOptions: {
//         tabBarLabel: "Home",
//         tabBarIcon: ({ tintColor }) =>
//           <Icon name="home" type='FontAwesome' style={{ color: tintColor }} />,
//       }
//     },
//     dayoff: {
//       screen: DayOffScreen,
//       navigationOptions: {
//         tabBarLabel: "Nghỉ phép",
//         tabBarIcon: ({ tintColor }) =>
//           <Icon name="tired" type='FontAwesome5' style={{ color: tintColor, fontSize: 22, }} />,
//       }
//     },
//     // Our plus button
//     checkin: {
//       screen: () => null, // Empty screen
//       navigationOptions: () => ({
//         tabBarLabel: " ",
//         tabBarIcon: <CheckinButton />
//       })
//     },
//     overtime: {
//       screen: OverTimeScreen,
//       navigationOptions: {
//         tabBarLabel: "Overtime",
//         tabBarIcon: ({ tintColor }) =>
//           <Icon type='FontAwesome5' name="user-clock" style={{ color: tintColor, fontSize: 19, }} />,
//       }
//     },
//     account: {
//       screen: AccountScreen,
//       navigationOptions: {
//         tabBarLabel: "Tài khoản",
//         tabBarIcon: ({ tintColor }) =>
//           <Icon type='FontAwesome' name="id-card" style={{ color: tintColor, fontSize: 19 }} />,
//       }
//     },
//   },
//   {
//     tabBarComponent: props => (
//       <TabBarComponent {...props} style={{
//         shadowColor: 'rgba(0,0,0, .4)', // IOS
//         shadowOffset: { height: 1, width: 1 }, // IOS
//         shadowOpacity: 1, // IOS
//         shadowRadius: 1, //IOS
//         elevation: 2, // Android
//         height: 55,
//         justifyContent: 'center',
//         alignItems: 'center',
//         flexDirection: 'row',
//         borderTopColor: 'transparent',
//       }} />
//     ),
//   }
// )

// const RootNavigator = createStackNavigator(
//   {
//     splash: SplashScreen,
//     home: TabNavigatorComponent,
//     detailNews: DetailNewsScreen,
//     rule: RuleScreen,
//     allowance: AllowanceMgrScreen,
//     approveOvertime: ApproveOverTimeScreen,
//     login: LoginScreen,
//     employees: EmployeeScreen,
//     checkincheckout: CheckinScreen,
//     myProfile: MyProfileScreen,
//     changePassword: ChangePasswordScreen,
//     registerOT: RegisterOTScreen,
//     registerDayOff: RegisterDayOffScreen
//   },
//   {
//     initialRouteName: 'splash',
//     headerMode: 'none',
//     header: null,
//     gesturesEnabled: false,
//     navigationOptions: {
//       header: null,
//       gesturesEnabled: false,
//     },
//     // mode: Platform.OS == "ios" ? "modal" : "card"
//     transitionConfig: nav => handleCustomTransition(nav),
//   },
// );
// let AppContainer = createAppContainer(RootNavigator);
// export { AppContainer };
