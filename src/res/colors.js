export default {
    defaultColor: '#0F69EF',
    black:'#111',
    white:'#fff',
    green:'rgb(6,112,90)',
    red:'rgba(209, 3, 3, 1)'
}