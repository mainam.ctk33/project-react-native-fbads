module.exports = {
    dependencies: {
        // '@react-native-community/viewpager': {
        //     platforms: {
        //         ios: null,
        //         android: null
        //     }
        // }
        "react-native-code-push": {
            platforms: {
                android: null // disable Android platform, other platforms will still autolink if provided
            }
        }
    },
    assets: [
        "./src/res/fonts/"
    ]
};